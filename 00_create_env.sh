#!/bin/bash
conda create -n mta-reporting ipykernel -y
source activate mta-reporting

conda install seaborn pandas matplotlib scipy

pip install fsspec gcsfs
pip install google.auth google.cloud
pip install --upgrade 'google-cloud-bigquery[bqstorage,pandas]'
pip install pandas-gbq
