# -*- coding: utf-8 -*-
"""
Overview
Usage
    Use MTA venv
    python extract_gcp_results.py [FW]
Author
    Andrew Lau
"""
EMAIL_RECIPIENT = 'alau3@woolworths.com.au'
# exceptions for fw - the latest safari run date has an error in GCP
EXCEPTIONS = {'2020-09-14':'2020-09-23', '2021-08-02':'2021-08-23', '2021-08-09':'2021-08-19', '2021-08-16':'2021-08-29',
    '2021-12-20':'2022-01-12'}
# these FWs have no data
SKIP = ['2020-06-15', '2020-06-08', '2020-06-01', "2020-05-25", '2020-12-21', '2020-12-28', '2020-05-25']

import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import time
import datetime
import scipy

# block to setup BQ
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

import email_notifications
# GCP authentication
# os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json" 
# Explicitly create a credentials object. This allows you to use the same
# credentials for both the BigQuery and BigQuery Storage clients, avoiding
# unnecessary API calls to fetch duplicate authentication tokens.
credentials, your_project_id = google.auth.default(
    scopes=["https://www.googleapis.com/auth/cloud-platform"]
)

# Make BQ clients.
bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)


fw_safari_run_mapping = {"2021-04-05":"2021-04-13",
                         "2021-03-29":"2021-04-08",
                         "2021-03-22":"2021-03-31",
                         "2021-03-15":"2021-03-24",
                         "2021-03-08":"2021-03-17",
                         "2021-03-08":"2021-03-17",
                         "2021-03-01":"2021-03-12"                         
                         }

def get_post_MTA_inc_sales(data_date=None, fw=None, campaign_code="ALL", groupby="channel", fpath_out=None, exceptions=EXCEPTIONS, skip=SKIP):
    """
    Overview
        Grabs the final Safari post-MTA results (including manual FBAA run) from GCP
    Arguments        
        data_date - date of MTA run, check gs://digital-attribution-data/phase2/markov_out/
        fw - financial week. If this is provided rather than data date, the function will get the latest valid
            safari run from the week after as the safari data date.
        campaign_code="ALL" - enter a campaign code to filter by a campaign
        groupby="channel" - group by field
        fpath_out=None - where to save outputs
    Returns
        Dataframe of grouped post-MTA results
    """
    print(fw)
    
    # exceptions for fw - the latest safari run date has an error in GCP
    # exceptions = {'2020-09-14':'2020-09-23', '2021-08-09':'2021-08-19'}
    # these FWs have no data
    # skip = ['2020-06-15', '2020-06-08', '2020-06-01', "2020-05-25", '2020-12-21', '2020-12-28', '2020-05-25']
    
    if fw in skip:
        print("\tthis fw is a skip exception, no data!")
        return
    
    if fw in exceptions:
        data_date = exceptions[fw]    
    
    df = None

    if data_date:  # if the Safari data date is known
        df = pd.read_csv('gs://digital-attribution-data/phase2/markov_out/' + data_date + '/mc_final_agg.csv')
    else:        
        datetime_object = datetime.datetime.strptime(fw, '%Y-%m-%d')                      
        
        for extra_days_to_check in range(7, 15): # check (Mon, Tue, Wed, Thu, Fri, Sat, Sun, Mon)        
            data_date = datetime_object + datetime.timedelta(days=extra_days_to_check)
            data_date = data_date.strftime("%Y-%m-%d")            
            try:
                df = pd.read_csv('gs://digital-attribution-data/phase2/markov_out/' + data_date + '/mc_final_agg.csv')
                print("\tFOUND safari run date", data_date)
            except FileNotFoundError:
                print("\tFor", data_date, "FileNotFoundError, trying next day")
                continue

    # exit if no DF found                
    if df is None:
        return None      
  
    if campaign_code == "ALL":
        mask = (df.segment == "Total")        
    else:
        mask = (df.campaign_code == campaign_code) & (df.segment == "Total")        
    df_group = df[mask].groupby(["banner", "campaign_code", groupby]).sum()
    df_group['attributed_inc_sales_percent'] = (df_group['attributed_inc_sales'] / df_group['attributed_inc_sales'].sum()) * 100
    if fpath_out:
        df_group.to_csv(fpath_out + "post_MTA_results_" + data_date + ".csv")
    return df_group

def get_safari_result_by_channel(fw):
    df = get_post_MTA_inc_sales(fw=fw, groupby="event", fpath_out=None)
    if df is None:
        return
    df.reset_index(inplace=True)
    # extract channel, medium, event from full MC node name
    event_extract = df.event.str.extract(r"(AlwaysOn_)?(display|email|rw_app|rw_app_push|google_other|sem|wow_web|rw_web_oap|video|youtube|audio|dx|FB|prog_img|prog_vdo|sem_LIA|wow_web_asset|IG)(_brand|_generic|_other|_shopping)?_(clk|open|imp|view|impsclicks)")
    # check everything assigned to a channel
    print('\tfound df.shape is', df.shape)
    if event_extract.loc[:,1].isnull().sum() != 0:
        print("WARNING! regex could not extract the below events:")
        print(df.loc[event_extract.loc[:,1].isnull(), :])
        assert event_extract.loc[:,1].isnull().sum() == 0

    df.loc[:, "channel"] = event_extract.loc[:, 0].fillna("") + event_extract.loc[:, 1]

    df = df.groupby(["banner", "campaign_code", "channel"]).sum().reset_index()
    df.insert(loc=0, column='fw', value=fw)
    
    return df


if __name__ == "__main__":
    today = datetime.date.today()
    last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
    fw = str(last_monday)

    CREATE_TABLE = False  # whether to create the table again from scratch
    
    FW_END = fw  # what week to parse GCP bucket values until
    # override
#     FW_END = '2021-08-02'
    
#     FW_START = "2020-04-27"  # this is the start of all GCP history, but some older weeks look nonsensical
    # FW_START = "2021-01-04"
    FW_START = "2020-06-29"
    # FW_START = "2021-07-26"
    FW_MAX = 999

    datetime_object = datetime.datetime.strptime(FW_START, '%Y-%m-%d')

    final_df = pd.DataFrame()
    
    # try:

    for weeks_to_add in range(FW_MAX):
        datetime_object_plus_weeks = datetime_object + datetime.timedelta(weeks=weeks_to_add)

        # break when reached last fw to parse
        if datetime_object_plus_weeks > datetime.datetime.strptime(FW_END, '%Y-%m-%d'):
            break

        fw = datetime_object_plus_weeks.strftime("%Y-%m-%d")
        df = get_safari_result_by_channel(fw)
        if df is None:  # no results for fws around xmas
            continue        
        final_df = final_df.append(df)

    final_df.reset_index(inplace=True)
    final_df = final_df.drop("index", axis=1)

    print(">>> Exporting to CSV")
    final_df.to_csv("output/mta-results-history-" + FW_END + ".csv")

    print(">>> Exporting to BQ")
    final_df.to_gbq('personal.AL_MTA_dashboard_tmp_ex_bws', project_id='wx-bq-poc', if_exists='replace')
    print("success.. exported to `wx-bq-poc.personal.AL_MTA_dashboard_tmp_ex_bws`")   


    # deleting supers and bigw and inserting again to not stuff up BWS' update    
    if CREATE_TABLE:
        query_string = """
        ------------------------------------------------------
        -- UPLOAD TO DASHBOARD
        ------------------------------------------------------
        create or replace table `wx-bq-poc.personal.AL_MTA_dashboard` as (        
            SELECT
                b.fm_start_date,
                concat(fw, '_', campaign_code) as key,
                a.*

            FROM `wx-bq-poc.personal.AL_MTA_dashboard_tmp_ex_bws` as a
            left join (
                select
                    distinct
                        fw_start_date,
                        fp_start_date as fm_start_date,
                from `wx-bq-poc.loyalty.dim_date_hist`
            ) as b
            on a.fw = cast(b.fw_start_date as string)
        );

        """
    else:  # deleting supers and bigw and inserting again to not stuff up BWS' update
        query_string = """
        ------------------------------------------------------
        -- UPLOAD TO DASHBOARD
        ------------------------------------------------------
        delete from `wx-bq-poc.personal.AL_MTA_dashboard`
        where banner='supers' or banner = 'bigw' or banner='supermarkets';

        insert into `wx-bq-poc.personal.AL_MTA_dashboard` (        
            SELECT
                b.fm_start_date,
                concat(fw, '_', campaign_code) as key,
                a.*

            FROM `wx-bq-poc.personal.AL_MTA_dashboard_tmp_ex_bws` as a
            left join (
                select
                    distinct
                        fw_start_date,
                        fp_start_date as fm_start_date,
                from `wx-bq-poc.loyalty.dim_date_hist`
            ) as b
            on a.fw = cast(b.fw_start_date as string)
        );
        """

    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )


#        email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT,
#                                                    subject='wow.bot🤖: 🗠✔️ MTA dashboard updated for fw ' +  str(FW_END),
#                                                    message_body="https://datastudio.google.com/u/0/reporting/47182796-a5f5-4401-afe0-c31d5dc6d2b6/page/PEWWC \n\n `wx-bq-poc.personal.AL_MTA_dashboard`")


    # adding current week mc_final_crn as 2999-12-31
    query_string = """
    insert into `wx-bq-poc.personal.AL_MTA_dashboard` (
        SELECT
            cast('2999-12-31' as DATE) as fm_start_date,
            concat('2999-12-31', '_', campaign_code) as key,
            '2999-12-31' as fw,
            banner,
            campaign_code,
                case
                    when channel like "%_AlwaysOn_display%" then "AlwaysOn_display"
                    when channel like "%_AlwaysOn_google_other%" then "AlwaysOn_google_other"
                    when channel like "%_AlwaysOn_sem%" then "AlwaysOn_sem"
                    when channel like "%_AlwaysOn_video%" then "AlwaysOn_video"
                    when channel like "%_AlwaysOn_wow_web%" then "AlwaysOn_wow_web"

                    when channel like "%FB%" then "FB"
                    when channel like "%IG%" then "IG"
                    when channel like "%display%" then "display"
                    when channel like "%email%" then "email"
                    when channel like "%rw_app%" then "rw_app"
                    when channel like "%rw_web_oap%" then "rw_web_oap"
                    when channel like "%wow_web%" then "wow_web"
                    when channel like "%video%" then "video"
                    when channel like "%wow_app%" then "wow_app"

                    -- catch all to identidy ungrouped channels
                    else concat("UNGROUPED: ", channel)
                end as channel,
            -1 as event_volume,
            count(distinct crn) as event_reach,
            -1 as event_converted_crn,
            -1 as medium_reach,
            -1 as medium_converted_crn,
            sum(attributed_total_sales) as attributed_total_sales,
            sum(attributed_inc_sales) as attributed_inc_sales,
            -1 as attributed_inc_sales_percent
        -- NOTE NEED TO CHANGE AFTER WX-BQ-POC migration
        FROM `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`
        where campaign_code != "ONLINE"
        group by 1,2,3,4,5,6
        order by 1,2,3,4,5,6
    );  
    """
    results = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )

    # except Exception as ex:  # e-mail if error happens        
    #         time.sleep(5)  # sleep to prevent gmail from blocking rapid message sending
    #         email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT,
    #                                                     subject='wow.bot🤖: 🗠❌ FAILED... MTA dashboard update for fw ' +  str(FW_END),
    #                                                     message_body="ERROR:" + str(type(ex)) + str(ex) + str(ex.args) + '\n\n if you are getting and UnboundLocalError, likely it is because no runs were found in that FW')


