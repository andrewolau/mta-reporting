
import os
import logging
import datetime

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage

FW_START = '2020-06-29'
FW_END = '2021-06-28'
FW_MAX = 200
BQ_PROJECT='gcp-wow-rwds-ai-safari-prod'

##################################################
# LOGGING
##################################################
# logging.basicConfig(level=logging.DEBUG)
logFormatter = logging.Formatter("%(asctime)s [%(levelname)-7.7s]  %(message)s")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)  # need to set root logger to the lowest level

##################################################
# BQ CREDS AND CLIENT
##################################################
# set user credentials to create BQ client object
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
os.environ['GOOGLE_CLOUD_PROJECT']=BQ_PROJECT
logging.info("os.environ['GOOGLE_CLOUD_PROJECT'] = " + str(os.environ['GOOGLE_CLOUD_PROJECT']))

# grab credentials from default login, use gcloud auth login
credentials, your_project_id = google.auth.default(
    scopes=["https://www.googleapis.com/auth/cloud-platform"]
)

# Make clients.
bqclient = bigquery.Client(credentials=credentials, project=BQ_PROJECT,)
bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

datetime_object = datetime.datetime.strptime(FW_START, '%Y-%m-%d')

##################################################
# LOOP
##################################################
for weeks_to_add in range(FW_MAX):
    datetime_object_plus_weeks = datetime_object + datetime.timedelta(weeks=weeks_to_add)
    # break when reached last fw to parse
    if datetime_object_plus_weeks > datetime.datetime.strptime(FW_END, '%Y-%m-%d'):
        logging.info("breaking loop as FW_END reached")
        break

    fw = datetime_object_plus_weeks.strftime("%Y-%m-%d")
    fw_no_dash = fw[:4] + fw[5:7] + fw[-2:]
    
    logging.info("running summarise_campaign_attribution for " + fw)
    
    query_string = """
    create or replace table `gcp-wow-rwds-ai-safari-prod.safari_campaign_attribution.bws_campaign_attribution_output_{fw_no_dash}` as (
        select * from `wx-bq-poc.digital_attribution_modelling.bws_campaign_attribution_output_{fw_no_dash}`;
    )
    """.format(fw=fw, fw_no_dash=fw_no_dash)

    logging.debug(str(query_string))

    df_bq = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
    )

    logging.info("run complete for {fw_no_dash}`".format(fw_no_dash=fw_no_dash))
