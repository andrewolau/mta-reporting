# README #

### What is this repository for? ###

* Updates MTA dashboard using GCP data  
https://datastudio.google.com/u/0/reporting/47182796-a5f5-4401-afe0-c31d5dc6d2b6/page/PEWWC  

* Version 1.0

### How do I get set up? ###

* Summary of set up: Git clone and run `00_create_env.sh`. To update the MTA dash with the latest FW's results run `01_run.sh`.
* Configuration: add your email to the variable EMAIL_RECIPIENTS at the top of `extract_gcp_results.py` to receive email notifications.  
* Deployment instructions: schedule as a Cron job each week

`crontab -u alau3 -e`

`0 0 * * 3-7 cd ~/mta-reporting; bash 01_run.sh > cron.log 2>&1`

### Who do I talk to? ###

* Repo owner or admin: Andrew Lau, Nash squad